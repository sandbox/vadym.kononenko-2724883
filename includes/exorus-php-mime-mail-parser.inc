<?php

const drush_bounce_cli__use__bounce_analyze_non_delivery_report = 1;
// Default debug mail text file path is: 'bounce_cli/eml/1.eml'.
const drush_bounce_cli__debug__get_mail_from_file = 0;
const drush_bounce_cli__debug__mail = 0;

class BounceCLIeXorusPhpMimeMailParser extends eXorus\PhpMimeMailParser\Parser {
  /**
   * Retrieve a Email Headers array
   * @return Array
   */
  public function getHeaders()
  {
    if (isset($this->parts[1])) {
      return $this->getPart('headers', $this->parts[1]);
    } else {
      throw new \Exception(
        'setPath() or setText() or setStream() must be called before retrieving email headers.'
      );
    }
  }

  /**
   * Retrieve a specified MIME part
   * @return String or Array
   * @param $type String, $parts Array
   */
  private function getPart($type, $parts)
  {
    return parent::getPart($type, $parts);
  }
}

/**
 * Callback: parse email text into report data.
 */
function drush_bounce_cli_get_report() {
  // Contruct MimeMailParser object.
  $Parser = new BounceCLIeXorusPhpMimeMailParser();

  /**
   * Choose input method
   */
    /* 'bounce_analyze_non_delivery_report' uses fulltext regex search.
     * It can not use structured mail data from 'Mailparse' PHP extension.
     * @todo: create custom 'analysis_callback' and use latest mail part to see original mail headers.
     * @see: 'hook_bounce_analyst()'
     */
  if (drush_bounce_cli__debug__get_mail_from_file) {
    // Specify the raw mime mail text (use it for debug).
    $Parser->setText( file_get_contents(drupal_get_path('module', 'bounce_cli') . '/eml/1.eml') );
  }
  else {
    if (drush_bounce_cli__use__bounce_analyze_non_delivery_report) {
      $Parser->setText( stream_get_contents(STDIN) );
    }
    else {
      // PHP file resource (stream) to the mime mail.
      $Parser->setStream(STDIN);
    }
  }

  /**
   * Get all the necessary data
   */
  // Get all headers at all.
  $headers = $Parser->getHeaders();

  // Add mail headers to report.
  $report[] = array(
    'data' => $headers,
    // Probably wrong, but better than nothing.
    'charset' => 'utf-8',
  );

  // Get body text.
  $text = '';

  if (drush_bounce_cli__use__bounce_analyze_non_delivery_report) {
    // This is not enough to use just mail body as it does not contain all information. 
    //$text = $Parser->getMessageBody();
    $text = $Parser->data;
  }
  else {
    $text = $Parser->parts;
  }

  // Add mail body to report.
  $report[] = array(
    // Mail data (string in the case of default 'bounce_analyze_non_delivery_report()' handler).
    'data' => $text,
    // Parser will decode charset to 'utf-8' (if method 'getMessageBody()' is used).
    'charset' => 'utf-8',
  );

  if (drush_bounce_cli__debug__mail) {
    // Debug mails through file dumping.
    if (module_exists('devel')) {
      drupal_debug($report, '$report');
    }
    else {
      drupal_set_message(t('Can not dump mail. Any dump method could not found.'), 'error');
    }
  }

  return $report;
}
