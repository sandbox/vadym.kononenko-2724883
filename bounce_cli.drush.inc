<?php
/**
 * @file
 * Integrates bounce_cli with drush.
 */

/**
 * Implements hook_drush_command().
 *
 * @See drush_parse_command() for a list of recognized keys.
 *
 * @return
 *   An associative array describing your command(s).
 *
 * @todo
 *   Extend commands list by 'bounce-cli-maidir' (bod) and 'bounce-cli-mailbox' (bob) command to parse local stored messages.
 *   Note: implement processed message removal logic into dedicated folder or delete them at all.
 * @see drush_core_php_script() implementation.
 */
function bounce_cli_drush_command() {
  $items = array();
  $items['bounce-cli-pipe'] = array (
    'description' => "Fill in analyst queue with a reports received through pipe.",
    'aliases' => array('bcp'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_DATABASE,
  );
  return $items;
}

/**
 * Implements hook_drush_help().
 */
function bounce_cli_drush_help($section) {
  switch ($section) {
    case 'drush:bounce-cli-pipe':
      return dt("Get report from pipe and write it to analyst queue.");
  }
}

/**
 * Command Callback: Analyse reports received through pipe.
 */
function drush_bounce_cli_pipe() {

  // Get libraries list.
  $libraries = _bounce_cli_libraries();

  $library = FALSE;

  foreach($libraries as $name => $label) {
    // Load next library in priority order.
    $library = libraries_load($name);

    if ($library['installed']) {
      // Dynamic handler loading.
      module_load_include('inc', 'bounce_cli', 'includes/' . $name);

      // Get report from loaded handler.
      $report = drush_bounce_cli_get_report();

      // Verify if report was prepared.
      if ($report && $report != '') {
        watchdog(
          'bounce_cli',
          'Retrieved @reports from the Return-Path account.',
          array(
            '@reports' => '1 potential non-delivery report',
          ),
          WATCHDOG_INFO
        );

        // Direct analyser call.
        bounce_process_non_delivery_report_queue_item($report);

        drush_log(dt('Report was analysed successfully.'), 'success');
      }
      else {
        drush_log(dt('Report was not prepared properly.'), 'error');
      }
    }
  }

  if (! ($library && $library['installed'])) {
    drush_log(dt('None library is not installed.'), 'error');
  }
}
